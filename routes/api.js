const express = require('express')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()

const Post = require('../controllers/post.js')
const post = new Post

const logger = (req,res,next) => {
    console.log('LOG: ${req.method} ${req.url')
    next()
}

const api = express.Router()

api.use(logger)
api.use(jsonParser)

/// post dijadikan grup route dengan middleware
api.get('/post', post.getPost) 
api.get('/post/:index', post.getDetailPost)
api.post('/post', jsonParser, post.insertPost)
api.put('/post/:index', jsonParser, post.updatePost)
api.delete('/post/:index', post.deletePost)

module.exports = api