const { successResponse } = require("../helpers/response.js") // ini function untuk simple codingan
const fs = require('fs')
const readPost = fs.readFileSync('./data/post.json', 'utf-8')

class Post { 
    constructor() {
        this.post = []
    }

    getPost = (req,res) => {
        this.post = JSON.parse(readPost)
        successResponse(
            res,
            200,
            this.post,
            { total: readPost.length}
        )  
    }

    getDetailPost = (req,res) => {
        const index = req.params.index
        this.post = readPost
        successResponse(res,200,readPost[index])
    }

    insertPost = (req,res) => {
        this.post = JSON.parse(readPost)

        const body = req.body

        const param = {
            "text": body.text,
            "created_at": new Date(),
            "username": body.username
        }

        this.post.push(param)

        fs.writeFileSync('./data/post.json', JSON.stringify(this.post)) 

        successResponse(res,202,param)
    }

    updatePost = (req,res) => {
        // this.post = JSON.parse(readPost)

        const index = req.params.index
        const body = req.body
        
        this.post[index].text = body.text
        this.post[index].username = body.username
        this.post[index].created_at = body.created_at

        // fs.writeFileSync('./data/post.json', JSON.stringify(this.post)) 
        successResponse(res, 200, this.post[index])
    }

    deletePost = (req,res) => {
        this.post = JSON.parse(readPost)
        const index = req.params.index
        
        this.post.splice(index, 1)
        fs.writeFileSync('./data/post.json', JSON.stringify(this.post)) 
        successResponse(res,200,null)
    }
}
module.exports = Post

// create, READ, UPDATE, delete / CRUD
