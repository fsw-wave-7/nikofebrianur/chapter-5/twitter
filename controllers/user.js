const { successResponse } = require("../helpers/response.js")

class User {
    constructor() {
        this.user = []
    }
        
    getUser = (req,res) => {
        successResponse(
            res,
            200,
            this.user, 
            {total:this.user.length})
    }

    getDetailUser = (req,res) => {
        const index = req.params.index
        successResponse(res,200,this.post[index])
    }

    addUser = (req,res) => {
        const body = req.body

        const param = {
            "namaLengkap": body.namaLengkap,
            "username":body.username,
            "password": body.password,
            "email": body.email
        }

        this.user.push(param)
        successResponse(res,202,param)
    }

    updateUser = (req,res) => {
        const index = req.params.index
        const body = req.body

        this.user[index].namaLengkap = body.namaLengkap
        this.user[index].username = body.username
        this.user[index].password = body.password
        this.user[index].email = body.email

        successResponse(res,200,this.user[index])
    }

    deleteUser = (req,res) => {
        const index = req.params.index
        this.user.splice(index, 1)
        successResponse(res, 200,null)
    }
}

module.exports = User