const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()

const path = require('path')

const api = require('./routes/api.js')

const app = express()
const port = 3000

/// Set view engine
app.set('view engine', 'ejs')

/// Load static files 
app.use(express.static(__dirname + '/public'))

/// third party middleware for logging
app.use(morgan('dev'))

/// load api routes
app.use('/api', api) /// '/api > prefix biar enak

/// untuk render html dengan view engine
app.get('/menu', function(req,res) {
    res.render(path.join(__dirname, './views/menu'))
})

app.get('/', function(req,res) {
    const name = req.query.name || 'Starbucks'
    res.render(path.join(__dirname, './views/index'), {
        name: name
    })
})

const User = require('./controllers/user.js')
const user = new User

app.get('/', function(req,res) {
    res.json({
        'hello': 'world'
    })
})

/// user 
app.get('/user/', user.getUser)
app.get('/user/:index', user.getDetailUser)
app.post('/user', jsonParser, user.addUser)
app.put('/user/:index', jsonParser, user.updateUser)
app.delete('/user/:index', user.deleteUser)

/// handling error, contoh tampilan error = hilangkan tanda petik di world di atas 
// app.use(function(err, req, res, next) {
//     res.status(500).json({
//         status: 'fail',
//         errors: err.message
//     })
// })

/// untuk handle 404 tanpa parameter err
app.use(function(req, res, next) {
    res.status(404).json({
        status: 'fail',
        errors: 'Are you lost, buddy?'
    })
})

//cek server
app.listen(port, () => {console.log("Server ON cuy!") })
