const messages = {
    "200": "Sukses",
    "201": "Data saved"
}

function successResponse(
        res,
        code,
        data,
        meta = {}
) {
    res.status(code).json({
        data: data,
        meta: {
            code: code,
            message: messages[code.toString()],
            ...meta // spread operator
        }
    })
}

module.exports = {
    successResponse
}